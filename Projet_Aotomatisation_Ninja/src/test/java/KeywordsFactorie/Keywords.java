/* Groupe: 1203
 * Cours: AUTOMATISATION DES TESTS II
 * Projet: Ninja
 * Etudiants (Nom & Prénom):
 * 			// Leila Fodil
 * 			// Nadia Jlidi
 * 			// Abir Doussouki
 * 			// Faiza Mehenni
 * 			// Yacine Bazizi
 * 			// Hamdi Bannani
 * 			// Ouassim Bellout
 * */
package KeywordsFactorie;

import static org.testng.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import LocatorsFactorie.Locators;

public class Keywords {
	WebDriver driver;

//---------------------------------------------------------------------------------------------------------------------------
	// Constructeurs :
	
	public Keywords(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

//---------------------------------------------------------------------------------------------------------------------------

	// Les Methodes [KEYWORDS]:

	public int register(String abonnementNewsletter) throws IOException, InterruptedException {
		// Load variable file :
		File propfile = new File("data\\Variables.properties");
		Properties file = new Properties();
		FileInputStream fis = new FileInputStream(propfile);
		file.load(fis);
		// Appel de la class Locators :
		Locators locator = new Locators(driver);

		// Se rendre à la page s'enregistrer:
		locator.getHeaderMyAccount().click();
		locator.getRegisterMyAccount().click();
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");

		// Saisi du formulaire d'enregistrement:
		JavascriptExecutor js = (JavascriptExecutor) driver;

		locator.getFirstNameRegister().sendKeys(file.getProperty("vFirstName"));
		js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
				locator.getFirstNameRegister());

		locator.getLastNameRegister().sendKeys(file.getProperty("vLastName"));
		js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
				locator.getLastNameRegister());

		locator.getEmailRegister().sendKeys(file.getProperty("vEmail"));
		js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
				locator.getEmailRegister());

		locator.getTelephoneRegister().sendKeys(file.getProperty("vTeleohone"));
		js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
				locator.getTelephoneRegister());

		locator.getPasswordRegister().sendKeys(file.getProperty("vPassword"));
		js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
				locator.getPasswordRegister());

		locator.getConfirmPasswordRegister().sendKeys(file.getProperty("vPasswordConfirm"));
		js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
				locator.getConfirmPasswordRegister());
		// Opter pour un abonnement avec Newsletter ou non:
		String pathPart1 = "//div[@id='content']/form[@action='http://tutorialsninja.com/demo/index.php?route=account/register']//div[@class='form-group']/div[@class='col-sm-10']/label[";
		String pathPart2 = "]/input[@name='newsletter']";

		if (abonnementNewsletter == "no") {
			driver.findElement(By.xpath(pathPart1 + "2" + pathPart2)).click();
		}
		if (abonnementNewsletter == "yes") {
			driver.findElement(By.xpath(pathPart1 + "1" + pathPart2)).click();
		}

		locator.getCheckboxRegister().click();
		locator.getSubmitRegister().click();

		int userIsCreated = driver.findElements(By.xpath("//h1[contains(.,'Your Account Has Been Created!')]")).size();
		if (userIsCreated == 1) {
			String textCreate = driver.findElement(By.xpath("//h1[contains(.,'Your Account Has Been Created!')]"))
					.getText();
			System.out.println(textCreate);
			Thread.sleep(1500);
			locator.getContinueRegister().click();
			Thread.sleep(1000);
			locator.getHeaderMyAccount().click();
			Thread.sleep(1000);
			locator.getLogoutRegister().click();
			Thread.sleep(1000);
			String textLogout = driver.findElement(By.xpath("//div[@id='content']/h1[.='Account Logout']")).getText();
			Thread.sleep(1000);
			System.out.println(textLogout);
			Thread.sleep(1000);
			locator.getContinueLogoutRegister().click();
			Thread.sleep(1000);

		}

		int userIsNotCreated = driver
				.findElements(
						By.xpath("//div[@id='account-register']/div[@class='alert alert-danger alert-dismissible']"))
				.size();
		if (userIsNotCreated == 1) {
			String textNotCreate = driver
					.findElement(By
							.xpath("//div[@id='account-register']/div[@class='alert alert-danger alert-dismissible']"))
					.getText();
			System.out.println(textNotCreate);
			Thread.sleep(2000);
			if (textNotCreate.contains("Warning: E-Mail Address is already registered!")) {
				System.out.println("Vous essayez de crée un compte en double");
				Thread.sleep(1000);
			}

		}
		return userIsCreated;
	}

//---------------------------------------------------------------------------------------------------------------------------	

	public void login() throws IOException {
		// Load variable file :
		File propfile = new File("data\\Variables.properties");
		Properties file = new Properties();
		FileInputStream fis = new FileInputStream(propfile);
		file.load(fis);

		// Appel de la class Locators :
		Locators locator = new Locators(driver);

		// Se rendre à la page s'enregistrer:
		locator.getHeaderMyAccount().click();
		locator.getLoginMyAccount().click();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		locator.getEmailRegister().sendKeys(file.getProperty("vEmail"));
		js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
				locator.getEmailRegister());

		locator.getPasswordRegister().sendKeys(file.getProperty("vPassword"));
		js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
				locator.getPasswordRegister());

		locator.getSubmitRegister().click();

		int accountValide = driver.findElements(By.xpath("//h2[contains(.,'My Account')]")).size();
		if (accountValide == 1) {
			System.out.println("Le compte est validé");
		} else {
			System.out.println("le compte n'est pas valide");
		}

	}

//---------------------------------------------------------------------------------------------------------------------------

	public void logout() throws IOException, InterruptedException {
		// Appel de la class Locators :
		Locators locator = new Locators(driver);

		// Se deconnecter :
		Thread.sleep(1000);
		locator.getHeaderMyAccount().click();
		Thread.sleep(1000);
		locator.getLogoutRegister().click();
		Thread.sleep(1000);
		locator.getContinueLogoutRegister().click();
	}

//---------------------------------------------------------------------------------------------------------------------------

	public void modifieAccount() throws IOException, InterruptedException {
		// Load variable file :
		File propfile = new File("data\\Variables.properties");
		Properties file = new Properties();
		FileInputStream fis = new FileInputStream(propfile);
		file.load(fis);
		// Appel de la class Locators :
		Locators locator = new Locators(driver);

		SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy-HHmmss");
		Date date = new Date();

		locator.getHeaderMyAccount().click();
		locator.getLoginMyAccount().click();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		locator.getEmailRegister().sendKeys(file.getProperty("vEmail"));
		js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
				locator.getEmailRegister());

		locator.getPasswordRegister().sendKeys(file.getProperty("vPassword"));
		js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
				locator.getPasswordRegister());

		locator.getSubmitRegister().click();

		int accountValide = driver.findElements(By.xpath("//h2[contains(.,'My Account')]")).size();
		if (accountValide == 1) {
			// Edit account:
			locator.getEditAccount().click();

			locator.getFirstNameRegister().sendKeys(s.format(date));
			js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
					locator.getFirstNameRegister());

			locator.getLastNameRegister().sendKeys(s.format(date));
			js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
					locator.getLastNameRegister());

			locator.getEmailRegister().sendKeys(s.format(date));
			js.executeScript("arguments[0].setAttribute('style', 'background: #CBF9A6; border: 2px solid #376115;');",
					locator.getEmailRegister());

			locator.getSubmitRegister().click();
			Thread.sleep(1000);
			locator.getHeaderMyAccount().click();
			Thread.sleep(1000);
			locator.getLogoutRegister().click();
			Thread.sleep(1000);
			locator.getContinueLogoutRegister().click();
			locator.getHeaderMainPage().click();
		} else {
			locator.getHeaderMainPage().click();
		}

	}

//---------------------------------------------------------------------------------------------------------------------------

		public void forgotYourPassword() throws IOException, InterruptedException {
			// Load variable file :
			File propfile = new File("data\\Variables.properties");
			Properties file = new Properties();
			FileInputStream fis = new FileInputStream(propfile);
			file.load(fis);
			// Appel de la class Locators :
			Locators locator = new Locators(driver);

			// Se rendre à la page s'enregistrer:
			locator.getHeaderMyAccount().click();
			locator.getLoginMyAccount().click();
			// Click sur le lien mot de passe oublier "Forgotten Password"
			locator.getForgotPasswordLogin().click();
			Thread.sleep(2000);
			
			// Ouvrir un nouveau onglet et se connecter sur le site de création de mail virtuel déja utilisé
			((JavascriptExecutor)driver).executeScript("window.open()");
		    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		    driver.switchTo().window(tabs.get(1));
		    
		    driver.get(file.getProperty("vCourielURL"));
			
		    // Saisir le couriel déja crée pour reset
		    Thread.sleep(2000);
		    driver.findElement(By.id("login")).sendKeys(file.getProperty("vEmail"));
		    Thread.sleep(1000);
		    driver.findElement(By.xpath("//i[@class='material-icons-outlined f36']")).click();
		    Thread.sleep(1000);
		    // Recuperer le nombre de mail initiale reçu [intCountMails]
		    String textCountMails = driver.findElement(By.xpath("//div[@id='nbmail']")).getText();
		    String CountMails = textCountMails.substring(0, 2).trim();
		    int intCountMails=Integer.parseInt(CountMails);
		    System.out.println(intCountMails);
		    
		    // Revenir à la page Ninja et saisir ce couriel
		    driver.switchTo().window(tabs.get(0));
		    Thread.sleep(1000);
		    locator.getEmailRegister().sendKeys(file.getProperty("vEmail"));
		    Thread.sleep(1000);
		    locator.getSubmitRegister().click();
		    Thread.sleep(1000);
		    
		    // Revenir à la page Yopmail actualiser
		    driver.switchTo().window(tabs.get(1));
		    Thread.sleep(4000);
		    driver.findElement(By.id("refresh")).click();
		    // Recuperer le nombre de mail reçu après le reset [intCountMails2]
		    String textCountMails2 = driver.findElement(By.xpath("//div[@id='nbmail']")).getText();
		    String CountMails2 = textCountMails2.substring(0, 2).trim();
		    int intCountMails2=Integer.parseInt(CountMails2);
		    System.out.println(intCountMails);
		    Thread.sleep(1000);
		    
		    if (intCountMails2 == intCountMails) {
		    	driver.quit();
		    	fail("Dommage Ninja ne peut pas faire un reset du mot de passe oublier");
		    }
		    
		    if (intCountMails2 == intCountMails+1) {
		    	System.out.println("Super Ninja peut faire un reset du mot de passe oublier");
		    }

		}
		
//---------------------------------------------------------------------------------------------------------------------------

		public void searchProducts() throws IOException, InterruptedException {
			// Load variable file :
			File propfile = new File("data\\Variables.properties");
			Properties file = new Properties();
			FileInputStream fis = new FileInputStream(propfile);
			file.load(fis);
			// Appel de la class Locators :
			Locators locator = new Locators(driver);

			// Rechercher un produit :
			locator.getSearchHeaderPage().sendKeys(file.getProperty("vProduct"));
			Thread.sleep(1000);
			locator.getButton_searchHeaderPage().click();
			Thread.sleep(1000);
			
			int articlesCount = driver.findElements(By.xpath("//div[@id='content']/div[3]/div")).size();
			System.out.println("Le produit *** "+file.getProperty("vProduct")+" *** retourne : "+articlesCount+" articles");
			Thread.sleep(1000);

		}
		
//---------------------------------------------------------------------------------------------------------------------------

		public int checkoutProducts() throws IOException, InterruptedException {
			int countProductNotAvailable=9;
			// Load variable file :
			File propfile = new File("data\\Variables.properties");
			Properties file = new Properties();
			FileInputStream fis = new FileInputStream(propfile);
			file.load(fis);
			// Appel de la class Locators :
			Locators locator = new Locators(driver);

			// Vérifier la disponibilité d'un produit :
			locator.getSearchHeaderPage().sendKeys(file.getProperty("vProduct"));
			Thread.sleep(1000);
			locator.getButton_searchHeaderPage().click();
			Thread.sleep(1000);
			
			// Vérifier si le produit retourne des résultats
			int articlesCount = driver.findElements(By.xpath("//div[@id='content']/div[3]/div")).size();
			// Si le le produit existe
			if (articlesCount > 0) {
				locator.getAddToCart1().click();
				Thread.sleep(1000);
				String textAddSuccess = driver.findElement(By.xpath("//div[contains(@class,'alert alert-success')]")).getText().trim();
				// S'assurer que le produit a bien été ajouter au panier
				if (textAddSuccess.contains("Success: You have added")) {
					locator.getCheckout().click();
					Thread.sleep(2000);
					countProductNotAvailable = driver.findElements(By.xpath("//div[contains(@class,'alert alert-danger')]")).size();
					// Le produit n'est pas disponible
					if (countProductNotAvailable  == 1) {
						System.out.println("Le produit *** "+file.getProperty("vProduct")+" *** n'est pas disponible");
					}
					// Le produit est disponible
					if (countProductNotAvailable == 0) {
						System.out.println("Le produit *** "+file.getProperty("vProduct")+" *** est disponible");
						Thread.sleep(1000);
						locator.getShoppingCart().click();
					}
				// Supprimer le produit du pannier	
				Thread.sleep(1000);
				locator.getSupprimAddCart().click();
				Thread.sleep(1000);
				driver.findElement(By.linkText("Continue")).click();
				}
			}
			// Le produit ne retourne aucun résultat
			else {
				System.out.println("Aucun résultats pour le produit *** "+file.getProperty("vProduct"));
			}
			return countProductNotAvailable;

		}
		
//---------------------------------------------------------------------------------------------------------------------------

		public void productOrder() throws IOException, InterruptedException {
			// Load variable file :
			File propfile = new File("data\\Variables.properties");
			Properties file = new Properties();
			FileInputStream fis = new FileInputStream(propfile);
			file.load(fis);
			// Appel de la class Locators :
			Locators locator = new Locators(driver);

			// Passer la commande du produit :
			locator.getSearchHeaderPage().sendKeys(file.getProperty("vProduct"));
			Thread.sleep(1000);
			locator.getButton_searchHeaderPage().click();
			Thread.sleep(1000);
			locator.getAddToCart1().click();
			Thread.sleep(1000);
			String textAddSuccess = driver.findElement(By.xpath("//div[contains(@class,'alert alert-success')]")).getText().trim();
			// S'assurer que le produit a bien été ajouter au panier
			if (textAddSuccess.contains("Success: You have added")) {
				locator.getCheckout().click();
				Thread.sleep(1000);
				}
			
			String listElements = "(//h4[@class='panel-title'])";
			int listMesFichiers = driver.findElements(By.xpath(listElements)).size();
			
			String linkText=null;
			String text = "Confirm Order";
			int i;
			int trouver = 0;
			for (i=1; i<=listMesFichiers; i++) {
				linkText = driver.findElement(By.xpath("("+listElements+")["+i+"]")).getText();
				if (linkText.contains(text)) {
					System.out.println("L'utilisateur peut passer une commande");
					trouver=1;
					break;
				}
			}
			
			if (trouver==0) {
				System.out.println("L'utilisateur ne peut pas passer une commande");
			}
			
			Thread.sleep(1000);
			locator.getShoppingCart().click();
			Thread.sleep(1000);
			locator.getSupprimAddCart().click();
			Thread.sleep(1000);
			driver.findElement(By.linkText("Continue")).click();

		}
		
//---------------------------------------------------------------------------------------------------------------------------
		
}
