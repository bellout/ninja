/* Groupe: 1203
 * Cours: AUTOMATISATION DES TESTS II
 * Projet: Ninja
 * Etudiants (Nom & Prénom):
 * 			// Leila Fodil
 * 			// Nadia Jlidi
 * 			// Abir Doussouki
 * 			// Faiza Mehenni
 * 			// Yacine Bazizi
 * 			// Hamdi Bannani
 * 			// Ouassim Bellout
 * */
package TestCases;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import KeywordsFactorie.Keywords;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Ninja {
	WebDriver driver;
	
//-------------------------------------------------------------------------------------------------
	// Cas de test 1 : Nouvelle Inscription sans Newsletter :
	
	@Test
	public void nouvelleInscriptionSansNewsletter() throws InterruptedException, IOException {
		// - Appel de la class Keyword
		Keywords keyword = new Keywords(driver);
		//1. Préconditions:
		keyword.modifieAccount();
		//2. Actions à réaliser:
		// - S'enregister [Please put 'yes' or 'no' for Newsletter]
		String withNewsletter = "no";
		int res = keyword.register(withNewsletter);
		//3. Critères de succès:
		if (res == 1) {
			keyword.login();
			keyword.logout();
		}
		//4. Post-conditions:
		// Pas de post-conditions
	}
//-------------------------------------------------------------------------------------------------
	
//Cas de test 2 : Nouvelle Inscription avec Newsletter : 
	
	@Test
	public void nouvelleInscriptionAvecNewsletter() throws InterruptedException, IOException {
		// - Appel de la class Keyword
		Keywords keyword = new Keywords(driver);
		//1. Préconditions:
		keyword.modifieAccount();
		//2. Actions à réaliser:
		// - S'enregister [Please put 'yes' or 'no' for Newsletter]
		String withNewsletter = "yes";
		int res = keyword.register(withNewsletter);
		//3. Critères de succès:
		if (res == 1) {
			keyword.login();
			keyword.logout();
		}
		//4. Post-conditions:
		// Pas de post-conditions
	}

//-------------------------------------------------------------------------------------------------
	
	//Cas de test 3 : Nouvelle Inscription avec un compte en double : 
	
	@Test
	public void noNouvelleInscriptionDouble() throws InterruptedException, IOException {
		// - Appel de la class Keyword
		Keywords keyword = new Keywords(driver);
		//1. Préconditions:

		//2. Actions à réaliser:
		// - S'enregister [Please put 'yes' or 'no' for Newsletter]
		String withNewsletter = "yes";
		int res = keyword.register(withNewsletter);
		//3. Critères de succès:
		if (res == 1) {
			keyword.login();
			keyword.logout();
		}
		//4. Post-conditions:
		// Pas de post-conditions
	}
	
//-------------------------------------------------------------------------------------------------
	
	//Cas de test 4 : Se connecter avec des informations d'identification valides : 
	
	@Test
	public void loginWithValideAccount() throws InterruptedException, IOException {
		// - Appel de la class Keyword
		Keywords keyword = new Keywords(driver);
		//1. Préconditions:

		//2. Actions à réaliser		&	3. Critères de succès:
		// - S'enregister [Please put 'yes' or 'no' for Newsletter]
		keyword.login();

		//3. Critères de succès:

		//4. Post-conditions:
		keyword.logout();
	}
	
//-------------------------------------------------------------------------------------------------
	
		//Cas de test 5 : Réinitialiser le mot de passe oublié : 
		
		@Test
		public void passwordReset() throws InterruptedException, IOException {
			// - Appel de la class Keyword
			Keywords keyword = new Keywords(driver);
			//1. Préconditions:
			//Avoir déja crée un compte
			//2. Actions à réaliser		&	3. Critères de succès:
			// - Faire un reset du password
			keyword.forgotYourPassword();

			//4. Post-conditions:
			keyword.logout();
		}
		
//-------------------------------------------------------------------------------------------------
		
		//Cas de test 6 : Rechercher des produits : 
		
		@Test
		public void rechercherProduits() throws InterruptedException, IOException {
			// - Appel de la class Keyword
			Keywords keyword = new Keywords(driver);
			//1. Préconditions:
			keyword.login();
			//Avoir déja crée un compte
			//2. Actions à réaliser		&	3. Critères de succès:
			// - Rechercher un produit
			keyword.searchProducts();
			//4. Post-conditions:
			keyword.logout();
		}
		
//-------------------------------------------------------------------------------------------------
		
		//Cas de test 7 : Informé lorsque le produit recherché n'est pas disponible : 
		
		@Test
		public void disponibilitePproduits() throws InterruptedException, IOException {
			// - Appel de la class Keyword
			Keywords keyword = new Keywords(driver);
			//1. Préconditions:
			//Avoir déja crée un compte
			keyword.login();
			//2. Actions à réaliser		&	3. Critères de succès:
			// - Vérifier si le produit existe
			keyword.checkoutProducts();
			//4. Post-conditions:
			keyword.logout();
		}
		
//-------------------------------------------------------------------------------------------------
		
		//Cas de test 8 : Passer une commande  
		
		@Test
		public void passerCommande() throws InterruptedException, IOException {
			// - Appel de la class Keyword
			Keywords keyword = new Keywords(driver);
			//1. Préconditions:
			//Avoir déja crée un compte
			keyword.login();
			// - Vérifier si le produit existe
			int x = keyword.checkoutProducts();
			//2. Actions à réaliser		&	3. Critères de succès:
			// - Si le produit existe
			if (x == 0) {
				keyword.productOrder();
			}
			//4. Post-conditions:
			keyword.logout();
		}
		
//-------------------------------------------------------------------------------------------------
		
	@BeforeClass
	public void setup() throws IOException {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		// Load variable file :
		File propfile = new File("data\\Variables.properties");
		Properties file = new Properties();
		FileInputStream fis = new FileInputStream(propfile);
		file.load(fis);
		driver.get(file.getProperty("vURL"));

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

}
